import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import { Offline, Online } from 'react-detect-offline'
import * as eventbus from 'eventbusjs'
import * as _ from 'lodash'
import * as promise from 'bluebird'
import './App.css'
import 'bulma/css/bulma.css'
import 'font-awesome/css/font-awesome.min.css'
import {
  isLoggedIn,
  decodeAuthToken,
  getAuthToken
} from './util/AuthService'
import {
  retrieveInformation,
  retrieveInterestedProfiles,
  setInterestedProfile,
  deleteProfile,
  retrievePostComparisons,
  getTrends,
  checkIfPrivateProfile,
} from './util/StatsService'
import NavigationBar from './components/NavigationBar'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Upgrade from './pages/Upgrade'
import SignUp from './pages/SignUp'
import Home from './pages/Home'
import Comparisons from './pages/Comparisons'
import Profile from './pages/Profile'
//import Trends from './pages/Trends'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      showUpgradeModal: false,
      showPrivateModal: false,
      loadingParts: 0,
      totalLoadingParts: 4
    }
  }
  completedLoadingPart () {
    this.setState({
      loadingParts: this.state.loadingParts + 1
    })
  }
  componentDidMount () {
    if (isLoggedIn()) {
      this.setState({loading: true})
      decodeAuthToken
        .then(data => {
          const userData = _(data.metadata.interestedProfiles).filter(e => e.type === 'self').first()
          const otherData = _(data.metadata.interestedProfiles).filter(e => e.type === 'competitor').value()
          const username = userData ? userData.id : ''
          this.completedLoadingPart()
          return this.setState({
            tier: data.metadata.tier,
            userData: userData,
            otherData: otherData,
            username: username
          })
        })
        .then(() => {
          // Load all interested profiles
          return promise.all([
            retrieveInterestedProfiles(getAuthToken())
              .then(data => {
                if (!data) return
                this.completedLoadingPart()
                this.setState({
                  userData: _(data).filter(e => e.type === 'self').first(),
                  otherData: _(data).filter(e => e.type === 'competitor').value()
                })
              }),
            retrievePostComparisons(this.state.username)
              .then(data => {
                if (!data) return
                this.completedLoadingPart()
                this.setState({
                  worstPosts: data.worstPosts,
                  bestPosts: data.bestPosts,
                  postAnalysisDiscrepancy: data.discrepancy
                })
              }),
            promise
              // note that the id will be available before the username is
              .map([this.state.username].concat(this.state.otherData.map(e => e.id)),
                  (username) => getTrends(username))
              .then(data => {
                this.completedLoadingPart()
                this.setState({
                  followerTrends: data.map(e => e.followerTrend),
                  mediaTrends: data.map(e => e.mediaTrend),
                  likesTrends: data.map(e => e.likesTrend)
                })
              })
          ])
        })
        .then(() => this.setState({loading: false}))
    }

    // redirect any non authentication path
    const notAuthPath = !window.location.href.match(/signup|login/)
    this.props.history.listen((location) => {
      const notAuthPath = !window.location.href.match(/signup|login/)
      if (notAuthPath && !isLoggedIn()) this.props.history.push('/login')
    })
    if (notAuthPath && !isLoggedIn()) this.props.history.push('/login')

    // event dispatcher
    const refreshUserData = () => retrieveInterestedProfiles(getAuthToken())
                              .then(data => {
                                this.setState({
                                  userData: _(data).filter(e => e.type === 'self').first(),
                                  otherData: _(data).filter(e => e.type === 'competitor').value()
                                })
                              })
    const refreshTrends = () => promise
                                  .map([this.state.username].concat(this.state.otherData.map(e => e.username)),
                                      (username) => getTrends(username))
                                  .then(data => {
                                    this.setState({
                                      followerTrends: data.map(e => e.followerTrend),
                                      mediaTrends: data.map(e => e.mediaTrend),
                                      likesTrends: data.map(e => e.likesTrend)
                                    })
                                  })
    eventbus.addEventListener('signup_successful', (data) => {
      this.props.history.push('/login')
    })
    eventbus.addEventListener('login_successful', (data) => {
      this.props.history.push('/')
      window.location.reload()
    })
    eventbus.addEventListener('selected_selfprofile', (data) => {
      // Retrieve user profile
      const userid = data.target
      this.setState({loading: true, 'loadingSelf':true})
      checkIfPrivateProfile(data.target)
        .then(data=>{
          if(data.private){
            this.setState({loading: false, 'loadingSelf':false})
            this._showPrivateModal()
          }else{
            promise.all([
              retrieveInformation(userid),
              setInterestedProfile(userid, 'self', getAuthToken())
            ])
            .then(() => refreshUserData())
            .then(() => refreshTrends())
            .then(() => this.setState({loading: false, 'loadingSelf':false}))
          }
      })
    })
    eventbus.addEventListener('selected_benchmarkprofile', (data) => {
      const allowedBenchMarkNumber = [1, 2, 4]
      if (this.state.otherData.length >= allowedBenchMarkNumber[+this.state.tier]) {
        this.setState({
          showUpgradeModal: true
        })
        return
      }
      const userid = data.target
      checkIfPrivateProfile(data.target)
        .then(data=>{
          if(data.private){
            this._showPrivateModal()
          }else{
            retrieveInformation(userid)
            setInterestedProfile(userid, 'competitor', getAuthToken())
              .then(() => refreshUserData())
              .then(() => refreshTrends())
          }
        })
    })
    eventbus.addEventListener('delete_benchmarkprofile', (data) => {
      deleteProfile(data.target, getAuthToken())
        .then(() => refreshUserData())
        .then(() => refreshTrends())
    })
  }
  _showUpgradeModal (e) {
    this.setState({showUpgradeModal: true})
  }
  _hideUpgradeModal (e) {
    this.setState({showUpgradeModal: false})
  }
  _showPrivateModal (e) {
    this.setState({showPrivateModal: true})
  }
  _hidePrivateModal (e) {
    this.setState({showPrivateModal: false})
  }
  render () {
    return (
      <div className='App'>
        <div className={'modal' + (this.state.showUpgradeModal ? ' is-active' : '')}>
          <div onClick={this._hideUpgradeModal.bind(this)} className='modal-background' />
          <div className='modal-card'>
            <header className='modal-card-head'>
              <p className='modal-card-title'>Oops</p>
              <button
                className='delete' aria-label='close'
                onClick={this._hideUpgradeModal.bind(this)} />
            </header>
            <section className='modal-card-body'>
              <div className='content'>
                You'll need to upgrade your tier to do that
              </div>
            </section>
            <footer className='modal-card-foot'>
              <NavLink exact to='/upgrade'>
                <div
                  style={{marginRight: '10px'}}
                  className='button is-info is-outlined'
                  onClick={this._hideUpgradeModal.bind(this)}>
                  <span>Go To Upgrade Page</span>
                </div>
              </NavLink>
              <button
                className='button is-danger'
                onClick={this._hideUpgradeModal.bind(this)}>Cancel</button>
            </footer>
          </div>
        </div>
        <div className={'modal' + (this.state.showPrivateModal ? ' is-active' : '')}>
          <div onClick={this._hidePrivateModal.bind(this)} className='modal-background' />
          <div className='modal-card'>
            <header className='modal-card-head'>
              <p className='modal-card-title'>Oops</p>
              <button
                className='delete' aria-label='close'
                onClick={this._hidePrivateModal.bind(this)} />
            </header>
            <section className='modal-card-body'>
              <div className='content'>
                We can only pull from public instagram profiles
              </div>
            </section>
            <footer className='modal-card-foot'>
              <button
                className='button is-danger'
                onClick={this._hidePrivateModal.bind(this)}>Cancel</button>
            </footer>
          </div>
        </div>
        <NavigationBar />
        <Offline>
          <div className='app-routes has-text-centered'>
            <p className='is-size-1 is-white'>
              You're disconnected from the internet
            </p>
          </div>
        </Offline>
        <Online>
          <div className='app-routes'>
            {
            !this.state.loading
            ? <div>
                <Route
                  exact path='/login'
                  component={Login} />
                <Route
                  exact path='/logout'
                  component={Logout} />
                <Route
                  exact path='/signup'
                  component={SignUp} />
                <Route
                  exact path='/upgrade'
                  component={Upgrade} />
                <Route
                  exact path='/profile'
                  component={Profile} />
                <Route
                  exact path='/'
                  component={() =>
                    <Home
                      userData={this.state.userData}
                      otherData={this.state.otherData}
                    />
                  }
                />
                <Route
                  exact path='/comparisons'
                  component={() =>
                    <Comparisons
                      username={this.state.username}
                      worstPosts={this.state.worstPosts}
                      bestPosts={this.state.bestPosts}
                      postAnalysisDiscrepancy={this.state.postAnalysisDiscrepancy}
                    />
                  }
                />
                {/*
                  <Route
                    exact path='/trends'
                    component={() =>
                      this.state.username
                      ? <Trends
                        usernameList={[this.state.username].concat(this.state.otherData.map(e => e.username))}
                        followerTrends={this.state.followerTrends}
                        mediaTrends={this.state.mediaTrends}
                        likesTrends={this.state.likesTrends}
                      /> : null
                    }
                  />
                */}
            </div>
            : <div className='container is-white' style={{textAlign: 'center'}}>
              <span className='icon'>
                <i className='fa fa-spinner fa-spin fa-3x fa-fw' />
              </span>
              <p>{this.state.loadingSelf ? 'Please wait up to 5 minutes while we load your profile' : 'Please Wait...'}</p>
              <p>Loading {`${this.state.loadingParts * 100 / this.state.totalLoadingParts}%`}</p>
            </div>
          }
          </div>
        </Online>
      </div>
    )
  }
}

export default App
