const hostname = `${window.location.protocol}//${window.location.hostname}:3030`

const getJson = (relativeurl) =>
                  fetch(`${hostname}${relativeurl}`)
                    .then(res => res.json())
                    .catch(err => {
                      console.log(err)
                      return null
                    })

const postJson = (relativeurl, body) =>
                  fetch(`${hostname}${relativeurl}`, {
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify(body)
                  })
                  .then(res => res.json())
                  .catch(err => {
                    console.log(err)
                    return null
                  })

const abbreviateNumber = (number) => {
    // from https://stackoverflow.com/questions/2685911/is-there-a-way-to-round-numbers-into-a-reader-friendly-format-e-g-1-1k
  var decPlaces = 1
  decPlaces = Math.pow(10, decPlaces)
  var abbrev = [ 'K', 'M', 'B', 'T' ]
  for (var i = abbrev.length - 1; i >= 0; i--) {
    var size = Math.pow(10, (i + 1) * 3)
    if (size <= number) {
      number = Math.round(number * decPlaces / size) / decPlaces
      if ((number === 1000) && (i < abbrev.length - 1)) {
        number = 1
        i++
      }
      number += abbrev[i]
      break
    }
  }
  return number
}

const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min // The maximum is exclusive and the minimum is inclusive
}

const clampNumber = (num, min, max) => {
  return num <= min ? min : num >= max ? max : num
}

export {
  hostname,
  getJson,
  postJson,
  abbreviateNumber,
  getRandomInt,
  clampNumber
}
