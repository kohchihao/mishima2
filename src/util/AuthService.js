import { hostname, postJson } from './UtilFunctions'

const getAuthToken = () => (localStorage.getItem('AuthToken'))

const isLoggedIn = () => (!!getAuthToken())

const decodeAuthToken = getAuthToken()
                        ? fetch(`${hostname}/user/profile?token=${getAuthToken()}`)
                          .then(res => {
                            res = res.json()
                            if (res.error) {
                              window.location.reload()
                              localStorage.clear()
                            }
                            return res
                          })
                          .catch(err => {
                            // error parsing the AuthToken
                            window.location.reload()
                            localStorage.clear()
                          })
                        : undefined

const verifyLoginCredentials = (email, password) =>
                                  postJson(`/login`, {
                                    email: email,
                                    password: password
                                  })

const registerLoginCredentials = (email, password) =>
                                    postJson(`/signup`, {
                                      email: email,
                                      password: password
                                    })

const saveAuthToken = (token) => (localStorage.setItem('AuthToken', token))

const removeAuthToken = () => {
  localStorage.clear()
  window.location.reload()
}

export {
  isLoggedIn,
  getAuthToken,
  decodeAuthToken,
  verifyLoginCredentials,
  saveAuthToken,
  registerLoginCredentials,
  removeAuthToken
}
