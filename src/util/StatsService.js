import * as promise from 'bluebird'
import * as _ from 'lodash'
import { getJson, postJson } from './UtilFunctions'

const retrieveInformation = (userid) => getJson(`/ig/api/user/${userid}/buffered/info`)

const retrieveInterestedProfiles = (token) => getJson(`/user/interestedProfiles?token=${token}`)

const checkIfPrivateProfile = (userid) => getJson(`/ig/api/user/${userid}/private`)

const searchInstagramForHandle = (searchTerm) => getJson(`/ig/api/search/q/${searchTerm || 'a'}`)

const setInterestedProfile = (userid, type, token) => (
                                postJson(`/user/interestedProfiles?token=${token}`, {
                                  profile: {
                                    id: userid,
                                    type: type
                                  }
                                })
                              )

const deleteProfile = (userid, token) => (
                        postJson(`/user/interestedProfiles/delete?token=${token}`, {
                          id: userid
                        })
                      )

const getHeatMap = (useridList, timeOffset, isLargeDataSet) => (
                    promise.map(
                      useridList,
                      (name) => (getJson(`/ig/api/stats/${name}/heatmap?offset=${timeOffset}${isLargeDataSet ? '&large=true' : ''}`)))
                  )

const retrieveBasicStats = (userid) => getJson(`/ig/api/stats/${userid}`)

const retrieveSimilarProfiles = (useridNumber) => getJson(`/ig/api/user/${useridNumber}/similar`)

const retrievePostComparisons = (userid) => getJson(`/ig/api/stats/${userid}/comparison/posts`)

const getLatestValue = (arr) => (_.last(arr)[1])

const countHashTags = (caption) => (caption && caption.match(/\W#/g)) ? caption.match(/\W#/g).length : 0

const countTaggedUsers = (caption, username) => (caption && caption.match(/\W@/g))
                                                ? caption.match(/\W@\S+/g).filter(w => !w.match(username)).length
                                                : 0

const getTrends = (userid) => getJson(`/ig/api/stats/${userid}/trends`)
export {
  checkIfPrivateProfile,
  retrieveInformation,
  retrieveInterestedProfiles,
  setInterestedProfile,
  searchInstagramForHandle,
  getHeatMap,
  retrieveBasicStats,
  retrieveSimilarProfiles,
  deleteProfile,
  retrievePostComparisons,
  getLatestValue,
  countHashTags,
  countTaggedUsers,
  getTrends
}
