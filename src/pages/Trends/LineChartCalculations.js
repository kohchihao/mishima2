import * as _ from 'lodash'
import * as moment from 'moment'
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
const days = _.range(1, 32) // Number of days in a month
const weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
const hours = ['1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12am', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm', '12pm']
const dateGroups = {
  months: months,
  days: days,
  weekdays: weekdays,
  hours: hours
}
const chartColors = [
  ['red', 'rgb(255, 99, 132)'],
  ['orange', 'rgb(255, 159, 64)'],
  ['yellow', 'rgb(255, 205, 86)'],
  ['green', 'rgb(75, 192, 192)'],
  ['blue', 'rgb(54, 162, 235)'],
  ['purple', 'rgb(153, 102, 255)'],
  ['grey', 'rgb(201, 203, 207)']
]
const momentify = (time) => moment.unix(time)

const fillInZeroes = (length, data) => {
  let temp = Array(length).fill(0)
  data.forEach(e => {
    temp[(+e[0]) - 1] = e[1]
  })
  return temp
}

const fillInEmptyWithAverage = (data)=>{
  // We fill in empty spaces in between so it doesnt look that awkward
  let tempIndex = data.map((v,i)=>[v,i]).filter(v=>!!v[0])
  tempIndex.forEach((ele, i)=>{
    const nextEle = tempIndex[(+i)+1]
    const numberOfSpacesToFill = nextEle ? nextEle[1]-ele[1]-1 : 0
    if(numberOfSpacesToFill>0){
      const avg = Array(numberOfSpacesToFill).fill((ele[0]+nextEle[0])/2)
      data.splice(ele[1]+1, numberOfSpacesToFill, ...avg)
    }
  })
  return data
}

const transformFollowerTrendToDataset = (followerTrend, timeType) => {
  const filterMethod = [
    _(followerTrend)
      .filter(e => moment().diff(momentify(e[0]), 'hours') < 25)
      .groupBy(e => momentify(e[0]).hour()),
    _(followerTrend)
      .filter(e => moment().diff(momentify(e[0]), 'days') < 32)
      .groupBy(e => momentify(e[0]).date()),
    _(followerTrend)
      .filter(e => moment().diff(momentify(e[0]), 'months') < 12)
      .groupBy(e => momentify(e[0]).month() + 1) // momentjs months are 0-indexed
  ]
  const timeTypeArr = [hours, days, months][timeType]
  followerTrend = filterMethod[timeType].map((v, k) => [k, _.maxBy(v, v => v[0])[1]])
  return fillInEmptyWithAverage(fillInZeroes(timeTypeArr.length, followerTrend.value()))
}

const transformLikeTrendToDataset = (likeTrend, timeType) => {
  const filterMethod = [
    _(likeTrend)
        .map(post => _(post)
                    .filter(e => moment().diff(momentify(e[0]), 'hours') < 25)
                    .groupBy(e => momentify(e[0]).hour())),
    _(likeTrend)
        .map(post => _(post)
                    .filter(e => moment().diff(momentify(e[0]), 'days') < 32)
                    .groupBy(e => momentify(e[0]).date())),
    _(likeTrend)
        .map(post => _(post)
                    .filter(e => moment().diff(momentify(e[0]), 'months') < 12)
                    .groupBy(e => momentify(e[0]).month() + 1))
  ]
  likeTrend = filterMethod[timeType]
                .map(post => _(post)
                            .map((v, k) => [k, _.maxBy(v, v => v[0])[1]])
                            .value())
                .flatten()
                .groupBy(e => e[0])
                .map((v, k) => [k, _.sumBy(v, f => f[1])])

  const timeTypeArr = [hours, days, months][timeType]
  return fillInEmptyWithAverage(fillInZeroes(timeTypeArr.length, likeTrend.value()))
}

const transformMediaTrendToDataset = (mediaTrend, timeType) => {
  const filterMethod = [
    _(mediaTrend)
        .filter(e => moment().diff(momentify(e), 'hours') < 25)
        .groupBy(e => momentify(e).hour()),
    _(mediaTrend)
        .filter(e => moment().diff(momentify(e), 'days') < 32)
        .groupBy(e => momentify(e).date()),
    _(mediaTrend)
        .filter(e => moment().diff(momentify(e), 'months') < 12)
        .groupBy(e => momentify(e).month())
  ]
  mediaTrend = filterMethod[timeType]
                .map((v, k) => [k, v.length])
  const timeTypeArr = [hours, days, months][timeType]
  return fillInZeroes(timeTypeArr.length, mediaTrend.value())
}

const transformData = (labels, datasets) => ({
  labels: labels,
  datasets: _.map(datasets, (dataset, index) => {
    if (dataset) {
      return {
        label: dataset.legend,
        fill: false,
        backgroundColor: chartColors[index % (chartColors.length)][1],
        borderColor: chartColors[index % (chartColors.length)][1],
        data: dataset.data
      }
    }
  })
})
const generateOptions = (title, xAxesTitle, yAxesTitle) => ({
  responsive: true,
  title: {
    display: true,
    text: title
  },
  tooltips: {
    mode: 'index',
    intersect: false
  },
  elements: {
    line: {
      tension: 0, // disables bezier curves
    }
  },
  hover: {
    mode: 'nearest',
    intersect: true
  },
  scales: {
    xAxes: [{
      display: true,
      scaleLabel: {
        display: true,
        labelString: xAxesTitle
      }
    }],
    yAxes: [{
      display: true,
      scaleLabel: {
        display: true,
        labelString: yAxesTitle
      }
    }]
  }
})

export {
  dateGroups,
  transformData,
  generateOptions,
  transformFollowerTrendToDataset,
  transformLikeTrendToDataset,
  transformMediaTrendToDataset
}
