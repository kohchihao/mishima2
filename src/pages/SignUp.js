import React, { Component } from 'react'
import { registerLoginCredentials } from '../util/AuthService'
import * as eventbus from 'eventbusjs'

export default class SignUp extends Component {
  styles={
    signUpForm:{
      padding:'30px',
      height:'90%',
    },
    label:{
      color:'white'
    },
    signUpButton:{
      textAlign:'center',
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      inputEmail: '',
      inputError:false,
      inputPassword:'',
      loading:false,
      successfulRedirect:false,
    }
  }
  _inputEmailHandler(e){
    this.setState({
      inputEmail: e.target.value.replace(/\s+/g,'')
    })
  }
  _inputPasswordHandler(e){
    this.setState({
      inputPassword: e.target.value.replace(/\s+/g,'')
    })
  }
  _signUpHandler(e){
    e.preventDefault()
    this.setState({loading:true})
    if(!this.state.inputEmail || !this.state.inputPassword) {
      this.setState({
        inputError:true,
        loading:false
      })
    }else{
      registerLoginCredentials(this.state.inputEmail, this.state.inputPassword)
        .then(data=>{
          this.setState({
            loading:false
          })
          if(data.error){
            this.setState({
              inputError:true,
            })
          }else{
            this.setState({
              successfulRedirect:true,
            })
            setTimeout(()=>(eventbus.dispatch('signup_successful', data)), 2000)
          }
        })
    }
  }
  render() {
    return (
      <div className='container is-vcentered is-form-container' style={this.styles.signUpForm}>
        <form className='control' onSubmit={this._signUpHandler.bind(this)}>
          <div className='field'>
            <label className='label' style={this.styles.label}>Email</label>
            <div className='control has-icons-left has-icons-right'>
              <input 
                className={'input'+(this.state.inputEmailError ? ' is-danger' : '')} 
                type='email'  value={this.state.inputEmail} 
                onChange={this._inputEmailHandler.bind(this)}/>
              <span className='icon is-small is-left'>
                <i className='fa fa-envelope'></i>
              </span>
              <span className='icon is-small is-right'>
                <i className='fa fa-asterisk'></i>
              </span>
            </div>
            <label className='label' style={this.styles.label}>Password</label>  
            <div className='control has-icons-left has-icons-right'>
              <input 
                className={'input'+(this.state.inputPasswordError ? ' is-danger' : '')} 
                type='password' value={this.state.inputPassword} 
                onChange={this._inputPasswordHandler.bind(this)}/>
              <span className='icon is-small is-left'>
                <i className='fa fa-key'></i>
              </span>
              <span className='icon is-small is-right'>
                <i className='fa fa-asterisk'></i>
              </span>
            </div>
          </div>
          {
            this.state.inputError ? 
            <div className='notification is-danger'>
              Please check if your password and email is correct
            </div> :
            <div></div>  
          }
          {
            this.state.successfulRedirect ? 
            <div className='notification is-success'>
              <p>Successfully created a new account! </p>
              <p>Redirecting you to login page in 2 seconds...</p>
            </div> :
            <div></div>  
          }
          <div className='is-hcentered'>
            <button 
              type='submit' 
              className={'button is-info'+(this.state.loading ? ' is-loading' : '')}>
              Start Free Trial
            </button>
          </div>
        </form>
      </div>
    )
  }
}
