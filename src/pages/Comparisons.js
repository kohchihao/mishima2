import React, { Component } from 'react'
import { TagCloud } from 'react-tagcloud'
import * as shortid from 'shortid'
import * as moment from 'moment'

import PostCard from './Comparisons/PostCard'
import PostCardMobile from './Comparisons/PostCardMobile'

export default class Comparisons extends Component {
  constructor (props) {
    super(props)
    console.log(props)
  }
  render () {
    let worstPostTags, bestPostTags
    const tagCloudOptions = {
      luminosity: 'bright',
      hue: 'blue'
    }
    if (this.props.postAnalysisDiscrepancy) {
      worstPostTags = this.props.postAnalysisDiscrepancy[0]
      bestPostTags = this.props.postAnalysisDiscrepancy[1]
    }
    if (!this.props.bestPosts && !this.props.worstPosts) {
      return (
        <div className='container is-white' style={{textAlign: 'center'}}>
          <span className='icon'>
            <i className='fa fa-spinner fa-spin fa-3x fa-fw' />
          </span>
          <p>Loading post analysis...</p>
        </div>
      )
    }
    return (
      <div className='container columns is-centered' style={{margin: 'auto'}}>
        <div className='column is-white has-text-centered'>
          <div className='level comparisons-title'>
            <p className='has-text-left'>
              <span className='is-size-5'>
                Your Recent 20 
              </span>
              &nbsp;
              <b className='is-size-3'>
                <u>Worst</u> 
              </b>
              &nbsp;
              <span className='is-size-5'>
                Posts
              </span>
              <br/>
              <span className='is-size-7'>
                (You may wish to post less of these photos)
              </span>
            </p>
            <p className='is-size-7 comparison-subtitle has-text-right'>Last updated at : {moment().format('MMMM Do YYYY')}</p>
          </div>
          <div className='columns is-multiline is-hidden-mobile'>
            {this.props.worstPosts && this.props.worstPosts.map(post =>
              <div className='column is-half is-centered' key={shortid.generate()}>
                <PostCard {...post} username={this.props.username} />
              </div>
            )}
          </div>
          <div className='bar is-hidden-desktop' >
            {this.props.worstPosts && this.props.worstPosts.map(post =>
              <div className='column' key={shortid.generate()}>
                <PostCardMobile {...post} username={this.props.username} />
              </div>
            )}
          </div>
          <br />
          <div className='box'>
            <p className='is-size-5 is-normal'>Image Analysis Results</p>
            {
              worstPostTags
              ? <TagCloud minSize={12}
                maxSize={30}
                colorOptions={tagCloudOptions}
                tags={worstPostTags}
                onClick={tag => console.log('clicking on tag:', tag)} />
              : <p className='is-normal'>Try again later</p>
            }
          </div>
        </div>
        <div className='divider' />
        <div className='column is-white has-text-centered'>
          <div className='level comparisons-title'>
            <p className='has-text-left'>
              <span className='is-size-5'>
                Your Recent 20 
              </span>
              &nbsp;
              <b className='is-size-3'>
                <u>Best</u> 
              </b>
              &nbsp;
              <span className='is-size-5'>
                Posts
              </span>
              <br/>
              <span className='is-size-7'>
                (You may wish to post more of these photos)
              </span>
            </p>
            <p className='is-size-7 comparison-subtitle has-text-right'>Last updated at : {moment().format('MMMM Do YYYY')}</p>
          </div>
          <div className='columns is-multiline is-hidden-mobile'>
            {this.props.bestPosts && this.props.bestPosts.map(post =>
              <div className='column is-half is-centered' key={shortid.generate()}>
                <PostCard {...post} username={this.props.username} />
              </div>
            )}
          </div>
          <div className='bar is-hidden-desktop' >
            {this.props.bestPosts && this.props.bestPosts.map(post =>
              <div className='column' key={shortid.generate()}>
                <PostCardMobile {...post} username={this.props.username} />
              </div>
            )}
          </div>
          <br />
          <div className='box'>
            <p className='is-size-5 is-normal'>Image Analysis Results</p>
            {
              bestPostTags
              ? <TagCloud minSize={12}
                maxSize={30}
                colorOptions={tagCloudOptions}
                tags={bestPostTags}
                onClick={tag => console.log('clicking on tag:', tag)} />
              : <p className='is-normal'>Try again later</p>
            }
          </div>
        </div>
        <div className='divider' />
      </div>
    )
  }
}
