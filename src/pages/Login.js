import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { verifyLoginCredentials, saveAuthToken } from '../util/AuthService'
import * as eventbus from 'eventbusjs'

export default class Login extends Component {
  styles={
    loginForm:{
      padding:'30px',
      height:'90%',
    },
    label:{
      color:'white'
    },
    loginButton:{
      textAlign:'center',
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      inputEmail: '',
      inputError:false,
      inputPassword:'',
      loading:false,
    }
  }
  _inputEmailHandler(e){
    this.setState({
      inputEmail: e.target.value.replace(/\s+/g,'')
    })
  }
  _inputPasswordHandler(e){
    this.setState({
      inputPassword: e.target.value.replace(/\s+/g,'')
    })
  }
  _loginHandler(e){
    e.preventDefault()
    this.setState({loading:true})
    if(!this.state.inputEmail || !this.state.inputPassword) {
      this.setState({
        inputError:true,
        loading:false
      })
    }else{
      verifyLoginCredentials(this.state.inputEmail, this.state.inputPassword)
        .then(data=>{
          this.setState({
            loading:false
          })
          if(data && data.error){
            this.setState({
              inputError:true,
            })
          }else if(!data){
            console.log('server not started')
          }else{
            saveAuthToken(data.token)
            eventbus.dispatch('login_successful', data)
          }
        })
    }

  }
  render() {
    return (
      <div className='container is-vcentered is-form-container' style={this.styles.loginForm}>
        <form className='control' onSubmit={this._loginHandler.bind(this)}>
          <div className='field'>
            <label className='label' style={this.styles.label}>Email</label>
            <div className='control has-icons-left has-icons-right'>
              <input 
                className={'input'+(this.state.inputEmailError ? ' is-danger' : '')} 
                type='email'  value={this.state.inputEmail} 
                onChange={this._inputEmailHandler.bind(this)}/>
              <span className='icon is-small is-left'>
                <i className='fa fa-envelope'></i>
              </span>
              <span className='icon is-small is-right'>
                <i className='fa fa-asterisk'></i>
              </span>
            </div>
            <label className='label' style={this.styles.label}>Password</label>  
            <div className='control has-icons-left has-icons-right'>
              <input 
                className={'input'+(this.state.inputPasswordError ? ' is-danger' : '')} 
                type='password' value={this.state.inputPassword} 
                onChange={this._inputPasswordHandler.bind(this)}/>
              <span className='icon is-small is-left'>
                <i className='fa fa-key'></i>
              </span>
              <span className='icon is-small is-right'>
                <i className='fa fa-asterisk'></i>
              </span>
            </div>
          </div>
          {
            this.state.inputError ? 
            <div className='notification is-danger'>
              Please check if your password and email is correct
            </div> :
            <div></div>  
          }
          <div className='level'>
            <div className='level-left'>
              <button 
                type='submit' 
                className={'button is-info'+(this.state.loading ? ' is-loading' : '')}>
                Login
              </button>
            </div>
            <div className='level-right'>
              <NavLink className='button is-primary' exact to='/signup'>
                <span>Sign Up</span>
              </NavLink>
            </div>
          </div>
        </form>
      </div>
    )
  }
}
