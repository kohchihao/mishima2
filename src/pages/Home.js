import React, { Component } from 'react'
import SearchDropdownInstagram from '../components/SearchDropdownInstagram'
import HeatMap from './Home/HeatMap'
import PersonalProfile from './Home/PersonalProfile'
import ProfileList from './Home/ProfileList'
import * as eventbus from 'eventbusjs'
export default class Home extends Component {
  componentDidMount () {
  }
  render () {
    if (!this.props.userData) {
      return (
        <div className='container is-white'>
          <SearchDropdownInstagram
            selectedCallback={(e) => eventbus.dispatch('selected_selfprofile', e)}
          />
        </div>
      )
    }
    return (
      <div className='container is-white has-text-centered'>
        <div className='container is-fluid'>
          <p className='is-size-4'>Past week stats</p>
          <div className='level is-hidden-mobile'>
            <p className='is-size-5'>Your Profile</p>
            <p className='is-size-5' style={{marginRight: '3em'}}>Your Benchmarks</p>
          </div>
          <div className='level'>
            <div className='level-left'>
              <p className='is-size-5 is-hidden-desktop'>Your Profile</p>
              <PersonalProfile
                fullName={this.props.userData.full_name}
                userId={this.props.userData.username}
                profilePicSrc={this.props.userData.profile_pic_url}
              />
            </div>
            <p className='is-size-5 is-hidden-desktop'>Your Benchmarks</p>
            <ProfileList
              userIdNumber={this.props.userData.id}
              userDataList={this.props.otherData}
            />
          </div>
        </div>
        <div className='container is-fluid'>
          <p className='is-size-4'>Likes vs Posting Day and Hour</p>
          <p className='is-size-7'>darker color indicates more likes</p>
          <HeatMap
            userIdList={[this.props.userData.username].concat(this.props.otherData.map(e => e.username))}
          />
        </div>
      </div>
    )
  }
}
