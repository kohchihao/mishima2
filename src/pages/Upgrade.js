import React, { Component } from 'react'

export default class Upgrade extends Component {
  render () {
    return (
      <div className='container is-white has-text-centered'>
        <p className='is-size-3'>
          Upgrade now to enjoy more benefits
        </p>
      </div>
    )
  }
}
