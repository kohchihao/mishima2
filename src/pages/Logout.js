import React, { Component } from 'react'
import { removeAuthToken } from '../util/AuthService'

export default class Logout extends Component {
  componentDidMount () {
    removeAuthToken()
  }
  render () {
    return (
      <div className='container'>
        <div className='notification is-success'>
          You've successfully been logged out.
        </div>
      </div>
    )
  }
}
