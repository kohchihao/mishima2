import React, { Component } from 'react'
import { retrieveBasicStats } from '../../util/StatsService'

export default class PersonalProfile extends Component {
  constructor(props){
    super(props)
    this.state={
      loading:false,
    }
  }
  componentDidMount() {
    this.setState({loading:true})
    retrieveBasicStats(this.props.userId)
      .then(data=>{
        if(data.error) return null
        this.setState({
          loading:false,
          currentFollowerCount: data.currentFollowerCount, 
          followerCountChange: data.followerCountChange, 
          currentTotalLikeCount: data.currentTotalLikeCount, 
          likeCountChange: data.likeCountChange, 
          currentTotalMediaCount: data.currentTotalMediaCount, 
          mediaCountChange: data.mediaCountChange, 
        })
      })
  }

  tabComponent = (headerType,current, change) => {
    const colorize = (number)=>(number===0 ? 'black' :
                                number>0 ? 'green' : 
                                'red')
    const indicator = (number)=>(number>=0 ? '+' : '')
    return (
      <div>
        <span className='icon' style={{float:'left', marginRight:'10px'}}>
          <i className={'fa fa-fw fa-'+headerType}></i> 
        </span>
        <p style={{color: colorize(change)}}>
          {`${current} (${indicator(change)+change}%)`}
        </p>
      </div>
    )
  }

  render() {
    if(this.state.loading){
      return (
        <div className="box" style={{height:'100%','width':'300px'}}>
          <article className="media">
            <div className="media-left has-text-centered">
              <figure className="icon">
                <i className='fa fa-spinner fa-spin fa-3x fa-fw'></i> 
              </figure>
            </div>
            <div className="media-content">
              <div className="content">
                Please wait while we retrieve data for {this.props.userId} from Instagram ...
                Come back in a minute.
              </div>
            </div>
          </article>
        </div>
      )
    }
    return (
      <div className="box">
        <article className="media">
          <div className="media-left has-text-centered">
            <figure className="image is-64x64">
              <img src={this.props.profilePicSrc} alt={this.props.profilePicSrc}/>
            </figure>
            {
              this.props.canDelete ?
              <div 
                className="button is-danger is-small"
                onClick={()=>this.props.deleteCallBack(this.props.userId)}>
                Remove
              </div> :
              <div></div>
            }
          </div>
          <div className="media-content">
            <div className="content">
              <strong style={{marginRight:'10px'}}>{this.props.fullName}</strong> 
              <a target="_blank" href={`https://www.instagram.com/${this.props.userId}`}>
                <small>{`@${this.props.userId}`}</small>
              </a>
              {this.tabComponent('users', this.state.currentFollowerCount, this.state.followerCountChange)}
              {this.tabComponent('heart', this.state.currentTotalLikeCount, this.state.likeCountChange)}
              {this.tabComponent('photo', this.state.currentTotalMediaCount, this.state.mediaCountChange)}
            </div>
          </div>
        </article>
      </div>
    )
  }
}
