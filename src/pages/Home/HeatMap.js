import React, { Component } from 'react'
import * as moment from 'moment'
import * as _ from 'lodash'
import { getHeatMap } from '../../util/StatsService'
import { initializeChart, heatMapChart } from './HeatMapChartCalculations'

export default class HeatMap extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      userIdAndData: [],
      activeUserId: this.props.userIdList[0]
    }
  }
  componentDidMount () {
    getHeatMap(this.props.userIdList, moment().local().utcOffset(), window.innerWidth > 600)
      .then((data) => {
        initializeChart()
        heatMapChart(data[0])
        this.setState({
          userIdAndData: _.zip(this.props.userIdList, data),
          loading: false
        })
      })
  }

  _setActiveUser (e) {
    const userid = e.target.getAttribute('data-userid')
    this.setState({
      activeUserId: userid
    })
    heatMapChart(this.state.userIdAndData.filter(e => e[0] === userid)[0][1])
  }

  render () {
    return (
      <div className='is-centered-desktop-only'>
        <style>{`
          rect.bordered {
            stroke: #E6E6E6;
            stroke-width:2px;
          }

          text.mono {
            font-size: 9pt;
            font-family: Consolas, courier;
            fill: #aaa;
          }

          text.axis-workweek {
            fill: #000;
          }

          text.axis-worktime {
            fill: #000;
          }

        `}</style>
        <div style={{marginTop: '10px', marginBottom: '10px'}}>
          {
            this.state.userIdAndData.map(e => e[0]).map(userid =>
              <div
                className={'button is-rounded ' + (this.state.activeUserId === userid ? ' is-dark' : 'is-light')}
                onClick={this._setActiveUser.bind(this)}
                data-userid={userid}
                key={userid}
              >
                {userid === this.props.userIdList[0] ? 'me' : userid}
              </div>
            )
          }
        </div>
        <div id='heat-map-chart' />
        {
          this.state.loading
          ? <div className='container is-white' style={{textAlign: 'center'}}>
            <span className='icon'>
              <i className='fa fa-spinner fa-spin fa-3x fa-fw' />
            </span>
            <p>Please Wait...</p>
          </div>
          : <div />
        }
      </div>
    )
  }
}
