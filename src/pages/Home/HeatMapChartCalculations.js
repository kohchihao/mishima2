import * as d3 from 'd3'
const days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
// mobile is smaller width
const isMobile = window.innerWidth < 600
const times = isMobile
              ? ['3a', '6a', '9a', '12a', '3p', '6p', '9p', '12p']
              : ['1a', '2a', '3a', '4a', '5a', '6a', '7a', '8a', '9a', '10a', '11a', '12a', '1p', '2p', '3p', '4p', '5p', '6p', '7p', '8p', '9p', '10p', '11p', '12p']
const buckets = 9
const width = isMobile ? 250 : window.innerWidth * 0.8
const gridSize = width / (times.length + 1)
const height = 8 * gridSize
const colors = ['#ffffd9', '#edf8b1', '#c7e9b4', '#7fcdbb', '#41b6c4', '#1d91c0', '#225ea8', '#253494', '#081d58']
const initializeChart = () => {
  d3.select('#heat-map-chart').append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr('transform', 'translate(' + 0 + ',' + (gridSize * 0.3) + ')')
}
const heatMapChart = (data, options) => {
  // We need to set offset right
  const svg = d3.select('#heat-map-chart svg g')
  svg
    .selectAll('.dayLabel')
    .data(days || options.yAxis)
    .enter().append('text')
      .text(function (d) { return d })
      .attr('x', 0)
      .attr('y', (d, i) => (i + 1) * gridSize)
        .attr('class', (d, i) => ((i >= 0 && i <= 4) ? 'dayLabel mono axis axis-workweek' : 'dayLabel mono axis'))

  svg
    .selectAll('.timeLabel')
    .data(times || options.xAxis)
    .enter().append('text')
      .text((d) => d)
      .attr('x', (d, i) => (i + 1) * gridSize)
      .attr('y', 0)
      .attr('class', (d, i) => ((i >= times.length * 2 / 8 && i <= times.length * 5 / 8) ? 'timeLabel mono axis axis-worktime' : 'timeLabel mono axis'))

  const colorScale = d3.scaleQuantile()
    .domain([0, buckets - 1, d3.max(data, (d) => d.value)])
    .range(colors)

  const cards = svg
                  .selectAll('.hour')
                  .data(data, (d) => d.day + ':' + d.hour)

  cards.append('title')
  cards
    .enter().append('rect')
      .attr('x', (d) => (isMobile ? ((d.hour / 3 + 1) * gridSize - 7) : ((d.hour + 1) * gridSize)))
      .attr('y', (d) => (d.day) * gridSize + 10)
      .attr('rx', 4)
      .attr('ry', 4)
      .attr('class', 'hour bordered')
      .attr('width', gridSize)
      .attr('height', gridSize)
      .style('fill', colors[0])
    .merge(cards)
      .transition()
      .duration(500)
      .style('fill', (d) => colorScale(d.value))

  cards.select('title').text((d) => d.value)

  cards.exit().remove()
}

export {initializeChart, heatMapChart}
