import React, { Component } from 'react'
import * as eventbus from 'eventbusjs'
import * as shortid from 'shortid'
import { getRandomInt, clampNumber } from '../../util/UtilFunctions'
import { retrieveSimilarProfiles } from '../../util/StatsService'
import PersonalProfile from './PersonalProfile'
import SimilarProfile from './SimilarProfile'
import DefaultProfile from './DefaultProfile'
import SearchDropdownInstagram from '../../components/SearchDropdownInstagram'

export default class ProfileList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      defaultCompetitorList: [],
      showSelectionModal: false,
      showConfirmationModal: false,
      showDeleteConfirmationModal: false,
      confirmationModalTarget: '',
      similarProfiles: [],
      loading: false
    }
  }
  componentDidMount () {
    console.log('->', this.props.userDataList)
    this.setState({loading: true})
    const defaultCompetitorList = [
        { default: true, id: getRandomInt(10, 1000) },
        { default: true, id: getRandomInt(10, 1000) },
        { default: true, id: getRandomInt(10, 1000) }
    ]
    const remainingSpace = clampNumber(3 - this.props.userDataList.length, 1, 3)
    const remainingDefaults = defaultCompetitorList.slice(0, remainingSpace)
    retrieveSimilarProfiles(this.props.userIdNumber)
      .then(data => {
        data = data.filter(e => (this.props.userDataList.map(e => e.id).indexOf(e.id + '') === -1))
        this.setState({
          similarProfiles: data,
          defaultCompetitorList: remainingDefaults,
          loading: false
        })
      })
  }
  _scrollLeft (e) {
    this.refs.bar.scrollLeft -= 250
  }
  _scrollRight (e) {
    this.refs.bar.scrollLeft += 250
  }
  _showSelectionModal (e) {
    this.setState({showSelectionModal: true})
  }
  _hideSelectionModal (e) {
    this.setState({showSelectionModal: false})
  }
  _showConfirmationModal (e) {
    this.setState({
      confirmationModalTarget: e,
      showConfirmationModal: true
    })
  }
  _hideConfirmationModal (e) {
    this.setState({showConfirmationModal: false})
  }
  _showDeleteConfirmationModal (e) {
    console.log(e)
    this.setState({
      confirmationModalTarget: e,
      showDeleteConfirmationModal: true
    })
  }
  _hideDeleteConfirmationModal (e) {
    this.setState({showDeleteConfirmationModal: false})
  }
  render () {
    return (
      <div className='level-right profile-list-container'>
        <a>
          <span
            className='icon is-white is-large is-hidden-mobile'
            onClick={this._scrollLeft.bind(this)}>
            <i className='fa fa-angle-left' />
          </span>
        </a>
        {
          this.state.loading
          ? <div className='container is-white' style={{textAlign: 'center'}}>
            <span className='icon'>
              <i className='fa fa-spinner fa-spin fa-3x fa-fw' />
            </span>
            <p>Please Wait...</p>
          </div>
          : <div
            ref='bar'
            className='bar'>
            {
              this.state.showSelectionModal
              ? <div className={'modal' + (this.state.showSelectionModal ? ' is-active' : '')}>
                <div onClick={this._hideSelectionModal.bind(this)} className='modal-background' />
                <div className='modal-content'>
                  <SearchDropdownInstagram
                    selectedCallback={(e) => this._showConfirmationModal(e)}
                  />
                  {
                    this.state.similarProfiles.map(e =>
                      <SimilarProfile
                        key={shortid.generate()}
                        fullName={e.full_name}
                        userId={e.username}
                        profilePicSrc={e.profile_pic_url}
                        onClick={() => (this._showConfirmationModal(e.username))}
                      />
                    )
                  }
                </div>
                <button onClick={this._hideSelectionModal.bind(this)} className='modal-close is-large' aria-label='close' />
              </div> : null
            }
            <div className={'modal' + (this.state.showConfirmationModal ? ' is-active' : '')}>
              <div onClick={this._hideConfirmationModal.bind(this)} className='modal-background' />
              <div className='modal-card'>
                <header className='modal-card-head'>
                  <p className='modal-card-title'>Please Confirm</p>
                  <button
                    className='delete' aria-label='close'
                    onClick={this._hideConfirmationModal.bind(this)} />
                </header>
                <section className='modal-card-body'>
                  <div className='content'>
                    <p className='is-normal'>
                      Add @{this.state.confirmationModalTarget} to list of benchmarks?
                    </p>
                  </div>
                </section>
                <footer className='modal-card-foot'>
                  <button
                    className='button is-success is-outlined'
                    onClick={() => eventbus.dispatch('selected_benchmarkprofile', this.state.confirmationModalTarget)}>Confirm</button>
                  <button
                    className='button is-danger'
                    onClick={this._hideConfirmationModal.bind(this)}>
                    Cancel
                  </button>
                </footer>
              </div>
            </div>
            <div className={'modal' + (this.state.showDeleteConfirmationModal ? ' is-active' : '')}>
              <div onClick={this._hideDeleteConfirmationModal.bind(this)} className='modal-background' />
              <div className='modal-card'>
                <header className='modal-card-head'>
                  <p className='modal-card-title'>Please Confirm</p>
                  <button
                    className='delete' aria-label='close'
                    onClick={this._hideDeleteConfirmationModal.bind(this)} />
                </header>
                <section className='modal-card-body'>
                  <div className='content'>
                    <p className='is-normal'>
                      Remove @{this.state.confirmationModalTarget} from list of benchmarks?
                    </p>
                  </div>
                </section>
                <footer className='modal-card-foot'>
                  <button
                    className='button is-success is-outlined'
                    onClick={() => eventbus.dispatch('delete_benchmarkprofile', this.state.confirmationModalTarget)}>Confirm</button>
                  <button
                    className='button is-danger'
                    onClick={this._hideDeleteConfirmationModal.bind(this)}>
                    Cancel
                  </button>
                </footer>
              </div>
            </div>
            {
              this.props.userDataList.map(e =>
                <div className='level-item is-pulled-left' key={shortid.generate()}>
                  <PersonalProfile
                    fullName={e.full_name}
                    userId={e.username}
                    profilePicSrc={e.profile_pic_url}
                    canDelete
                    deleteCallBack={this._showDeleteConfirmationModal.bind(this)}
                  />
                </div>
              )
            }
            {
              // this is the default profile
              this.state.defaultCompetitorList.map(e =>
                <DefaultProfile
                  onClick={this._showSelectionModal.bind(this)}
                  key={shortid.generate()} />
              )
            }
          </div>
        }
        <a>
          <span
            className='icon is-white is-large is-hidden-mobile'
            onClick={this._scrollRight.bind(this)}>
            <i className='fa fa-angle-right' />
          </span>
        </a>
      </div>
    )
  }
}
