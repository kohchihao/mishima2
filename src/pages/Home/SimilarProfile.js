import React, { Component } from 'react'

export default class SimilarProfile extends Component {
  render () {
    return (
      <div onClick={this.props.onClick}>
        <div className='box' style={{'width': '100%'}}>
          <article className='media'>
            <div className='media-left'>
              <span className='icon is-large'>
                <figure className='image'>
                  <img src={this.props.profilePicSrc} alt={this.props.profilePicSrc} />
                </figure>
              </span>
            </div>
            <div className='media-content'>
              <div className='level'>
                <p>
                  <strong style={{marginRight: '10px'}}>{this.props.fullName}</strong>
                  <a target='_blank' href={`https://www.instagram.com/${this.props.userId}`}>
                    <small>{`@${this.props.userId}`}</small>
                  </a>
                </p>
                <div className='button is-info is-inverted'>
                  Add
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    )
  }
}
