import React, { Component } from 'react'

export default class DefaultProfile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      defaultCompetitorList: [],
      loading: false
    }
  }
  render () {
    return (
      <div className='level-item' style={{maxWidth: '300px'}}>
        <a onClick={this.props.onClick} style={{'height': '100%'}}>
          <div className='box' style={{'height': '100%'}}>
            <article className='media'>
              <div className='media-left'>
                <span className='icon is-large'>
                  <i className='fa fa-question-circle-o' />
                </span>
              </div>
              <div className='media-content'>
                {
                  this.state.loading
                  ? <div className='container is-white' style={{textAlign: 'center'}}>
                    <span className='icon'>
                      <i className='fa fa-spinner fa-spin fa-3x fa-fw' />
                    </span>
                    <p>Please Wait...</p>
                  </div>
                  : <div />
                }
                <div className='content'>
                  <strong style={{marginRight: '10px'}}>Your Benchmark</strong>
                  <small>@izunaa</small>
                  <br />

                    Click here and find out who your followers also look at!
                </div>
              </div>
            </article>
          </div>
        </a>
      </div>
    )
  }
}
