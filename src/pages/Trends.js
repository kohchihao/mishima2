import React, { Component } from 'react'

import { 
  dateGroups, 
  transformData, 
  generateOptions,
  transformFollowerTrendToDataset,
  transformLikeTrendToDataset,
  transformMediaTrendToDataset,
} from './Trends/LineChartCalculations'
import {Line} from 'react-chartjs-2'

export default class Trends extends Component {
  leftTab = ['Followers', 'Likes', 'Media']
  rightTab = ['Day', 'Month', 'Year']
  constructor(props){
    super(props)
    this.state={
      data:transformData(dateGroups.months, []),
      options: generateOptions('Loading data...', 'Loading data...', 'Loading data...'),
      dataTypeIndex:0,
      dataTimeIndex:1,
    }
  }

  componentDidMount() {
    if(this.props.followerTrends && this.props.likesTrends && this.props.mediaTrends){
      this._transformData()
    }
  }

  _transformData(){
    const label = this.leftTab[this.state.dataTypeIndex]
    const tranformMethod = [
                            transformFollowerTrendToDataset, 
                            transformLikeTrendToDataset,
                            transformMediaTrendToDataset,
                           ][this.state.dataTypeIndex]
    const transformTarget = [
                              this.props.followerTrends,
                              this.props.likesTrends,
                              this.props.mediaTrends,
                            ][this.state.dataTypeIndex]
    const timestr = ['within the last 24 hours', 
                     'within the last 30 days', 
                     'within the last 12 months'][this.state.dataTimeIndex]
    const time = ['Hours in a day', 'Days in a month', 'Months in a year'][this.state.dataTimeIndex]
    const timeGroup = [dateGroups.hours, dateGroups.days, dateGroups.months][this.state.dataTimeIndex]
    let datasets = transformTarget
                    .map(data=>tranformMethod(data, this.state.dataTimeIndex))
                    .map((data,i)=>({
                        legend:this.props.usernameList[i],
                        data:data
                      }))
    this.setState({
      options:generateOptions(`Recorded ${label.toLowerCase()} ${timestr} for recent 20 posts`, time, label),
      data: transformData(timeGroup, datasets)
    })
  }

  _setActiveDataType(e){
    const index = +e.target.getAttribute('data-index')
    this.setState({dataTypeIndex:index}, this._transformData)
  }

  _setActiveDataTime(e){
    const index = +e.target.getAttribute('data-index')
    this.setState({dataTimeIndex:index}, this._transformData)
  }

  render() {
    return (
      <div className="container">
        <div className="box">
          <div className="tabs">
            <ul className="is-left">
              {this.leftTab.map((e,i)=>
                <li 
                  key={e+i}
                  className={this.state.dataTypeIndex===i ? 'is-active' : ''}
                  onClick={this._setActiveDataType.bind(this)}
                >
                  <a data-index={i}>{e}</a>
                </li>
              )}
            </ul>
            <ul className="is-right">
              {this.rightTab.map((e,i)=>
                <li 
                  key={e+i}
                  className={this.state.dataTimeIndex===i ? 'is-active' : ''}
                  onClick={this._setActiveDataTime.bind(this)}
                >
                  <a data-index={i}>{e}</a>
                </li>
              )}
            </ul>
          </div>
          <Line data={this.state.data} options={this.state.options} width={600} height={200}/>
        </div>
      </div>
    )
  }
}
