import React, { Component } from 'react'
import * as moment from 'moment'
import {
  getLatestValue,
  countHashTags,
  countTaggedUsers
} from '../../util/StatsService'
import { abbreviateNumber } from '../../util/UtilFunctions'

export default class PostCardMobile extends Component {
  render () {
    return (
      <div className='card'>
        <div className='card-image'>
          <a target='_blank' href={`https://www.instagram.com/p/${this.props.code}/`}>
            <figure className='image is-4by3'>
              <img src={this.props.display_src} alt='meh' />
            </figure>
          </a>
        </div>
        <div className='card-content' style={{minHeight: '130px'}}>
          <div className='content'>
            <div className='columns is-gapless'>
              <div className='column is-two-thirds'>
                <p className='is-size-7'>{moment.unix(this.props.date).format('DD-MM-YYYY')}</p>
              </div>
              <div className='column'>
                <p className='is-size-7'> {moment.unix(this.props.date).format('h:mma')}</p>
              </div>
            </div>
            <div className='has-text-centered'>
              <div className='columns is-gapless'>
                <small className='column is-half-absolute is-pulled-left'>
                  <span className='icon is-small'>
                    <i className='fa fa-fw fa-heart' />
                    <p className='is-size-6'>{abbreviateNumber(getLatestValue(this.props.likes))}</p>
                  </span>
                </small>
                <small className='column is-half-absolute is-pulled-right'>
                  <span className='icon is-small'>
                    <i className='fa fa-fw fa-comment' />
                    <p className='is-size-6'>{getLatestValue(this.props.comments)}</p>
                  </span>
                </small>
              </div>
              <p />
              <div className='columns is-gapless'>
                <small className='column is-half-absolute is-pulled-left'>
                  <span className='icon is-small'>
                    <i className='fa fa-fw fa-hashtag' />
                    <p className='is-size-6'>{countHashTags(this.props.caption)}</p>
                  </span>
                </small>
                <small className='column is-half-absolute is-pulled-right'>
                  <span className='icon is-small'>
                    <i className='fa fa-fw fa-user' />
                    <p className='is-size-6'>{countTaggedUsers(this.props.caption, this.props.username)}</p>
                  </span>
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
