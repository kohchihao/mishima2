import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import {isLoggedIn} from '../util/AuthService'

export default class NavigationBar extends Component {
  componentDidMount () {
    document.addEventListener('DOMContentLoaded', function () {
      // Get all 'navbar-burger' elements
      var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)

      // Check if there are any nav burgers
      if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
          $el.addEventListener('click', function () {
            // Get the target from the 'data-target' attribute
            var target = $el.dataset.target
            var $target = document.getElementById(target)

            // Toggle the class on both the 'navbar-burger' and the 'navbar-menu'
            $el.classList.toggle('is-active')
            $target.classList.toggle('is-active')
          })
        })
      }
    })
  }
  render () {
    return (
      <nav className='navbar is-fixed'>
        <div className='navbar-brand'>
          <NavLink className='navbar-item is-semitransparent' exact to='/' activeClassName='is-normal is-active'>
            <span>Izunaa</span>
          </NavLink>
          <div className='navbar-burger burger' data-target='navMenubd-example'>
            <span />
            <span />
            <span />
          </div>
        </div>

        <div id='navMenubd-example' className='navbar-menu'>
          {
            isLoggedIn()
            ? <div className='navbar-start'>
              {/*
                <NavLink className='navbar-item' exact to='/trends' activeClassName='is-active'>
                  <span>Trends</span>
                </NavLink>
              */}
              <NavLink className='navbar-item' exact to='/comparisons' activeClassName='is-active'>
                <span>Comparisons</span>
              </NavLink>
              <NavLink className='navbar-item' exact to='/upgrade' activeClassName='is-active'>
                <span>Upgrade</span>
              </NavLink>
            </div>
            : <div />
          }

          <div className='navbar-end'>
            <div className='navbar-item'>
              <div className='field is-grouped'>
                {
                  isLoggedIn()
                  ? <div>
                    <p className='control'>
                      <NavLink className='button is-info' exact to='/profile'>
                        <span>Settings</span>
                        <span className='icon'>
                          <i className='fa fa-cog' />
                        </span>
                      </NavLink>
                    </p>
                  </div>
                  : <p className='control'>
                    <NavLink className='button is-info is-outlined' exact to='/login'>
                      <span>Sign In</span>
                    </NavLink>
                  </p>
                }

              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}
