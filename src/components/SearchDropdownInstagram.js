import React, { Component } from "react"
import Select from 'react-select'
import 'react-select/dist/react-select.css'
import * as _ from 'lodash'
import { searchInstagramForHandle } from '../util/StatsService'

export default class SearchDropdownInstagram extends Component {
  _searchKeywordInstagram = _.debounce((searchTerm, callback) => {
    if(!searchTerm) callback(null, {options: []})
    searchInstagramForHandle(searchTerm)
      .then((result) => {
        callback(null, {options: result})
      })
      .catch((error) => {
        callback(error, null)
      })
  }, 400)

  _onClick(data){
    console.log(data)
    if(data) this.props.selectedCallback(data.value)
  }

  render() {
    return (
      <Select.Async
        autofocus
        placeholder='Find instagram handle here'
        loadOptions={this._searchKeywordInstagram}
        onChange={this._onClick.bind(this)}
      />
    )
  }
}